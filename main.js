const states = {
    pending: "Pending",
    resolved: "Resolved",
    rejected: "Rejected"
}

// Promise Class
class MyPromise{

    // Promise Constructor

    constructor(executer){

        /* A Function to return a new promise that will be resolved or rejected based on the 
         passed callback */

        const callTry = callback => {
            return MyPromise.try(()=>callback(this.value))
        }

        // Object to manage different states
        const members = {
            [states.resolved]:{
                state: states.resolved,
                then: callTry,
                catch: (_) => this
            },
            [states.rejected]:{
                state: states.rejected,
                then: (_) => this,
                catch: callTry
            },
            [states.pending]:{
                state: states.pending
            }
        }

        /* A function to update the promise with new state, then and catch as behaviour of
         then and catch will be different for resolved and rejected  */  
        const changeState = state => Object.assign(this, members[state])


        /* A function to change the state of promise and assign the value property based on
         passed arguments. It will also be responsible for ignoring multiple resolve/reject. */
        const apply = (value , state)=>{
            if(this.state == states.pending){
                this.value = value
                changeState(state)
            }
        }

        /* A function to resolve the state of the promise with the passed argument. This
        function along with reject will be passed as an argument to our executer function
        call. */
        const resolve = (value) => {
            
            /* Here we will check weather the value from which the promise is supposed to be 
            resolved is an instance of MyPromise. If thats the case we will unpack the value 
            from the passed promise and use that value to either resolve or reject our promise
            (Based on the state of passed promise) */

            if(value instanceof MyPromise){
                value.then(value => apply(value , states.resolved))
                value.catch(value => apply(value , states.rejected))
            }else{
                apply(value , states.resolved)
            }
            
        }

        /* A function to reject the state of the promise with the passed argument. This
        function along with resolve will be passed as an argument to our executer function
        call. */

        const reject = (error) => {
            apply(error , states.rejected)
        }

        // Initializing the promise with pending state
        changeState(states.pending)

        /* Our Constructor will run the executer function and pass resolve and reject function
        definitions as an argument to resolve or reject the promise after a certain asynchronous
        operation. */
        try{
            executer(resolve , reject)
        }
        catch(err){
            reject(err)
        }
    }


    // Static Functions 

    /* A static function that returns a new Promise that will be resolved or rejected with the 
    value returned by the passed callback. Behaviour of then and catch according to 
    the state of promise will play a crucial role */
    static try(callback){
        return new MyPromise(resolve => resolve(callback()))
    }

    /* A static function that manually return a new promise that will be resolved by the passed
    argument */
    static Manualresolve(value){
        return new MyPromise(resolve => resolve(value))
    }

    /* A static function that manually return a new promise that will be rejected by the passed
    argument */
    static ManualReject(value){
        return new MyPromise(( _ , reject) => reject(value))
    }
}



